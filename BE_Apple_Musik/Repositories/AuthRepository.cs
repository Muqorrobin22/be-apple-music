﻿using BE_Apple_Musik.DTO.Auth;
using BE_Apple_Musik.Models;
using MySql.Data.MySqlClient;

namespace BE_Apple_Musik.Repositories
{
    public class AuthRepository
    {
        public string _connectionString;

        public AuthRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public User? GetByEmail(string email)
        {
            User? user = null;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                // able to query after open
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT user_id, email FROM users WHERE email=@Email", conn);
                cmd.Parameters.AddWithValue("@Email", email);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user = new User();
                    user.user_id = reader.GetInt32("user_id");
                    user.email = reader.GetString("email");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            //required
            conn.Close();

            return user;
        }

        public string Register(RegisterUserDTO data, string hashedPassword, string verificationToken)
        {
            string errorMessage = string.Empty;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connectionString);

            conn.Open();

            MySqlTransaction transaction = conn.BeginTransaction();

            try
            {
                MySqlCommand cmd = new MySqlCommand("INSERT INTO users(first_name, last_name, email, password, verification_token, verification_expired_date, role_id) VALUES (@FirstName, @LastName, @Email, @Password, @VerificationToken, @VerificationExpiredDate, @RoleId)", conn);
                cmd.Transaction = transaction;
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Password", hashedPassword);
                cmd.Parameters.AddWithValue("@RoleId", 1);
                cmd.Parameters.AddWithValue("@VerificationToken", verificationToken);
                cmd.Parameters.AddWithValue("@VerificationExpiredDate", DateTime.Now.AddMinutes(30));
                cmd.Parameters.AddWithValue("@FirstName", data.first_name);
                cmd.Parameters.AddWithValue("@LastName", data.last_name);
                cmd.ExecuteNonQuery();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                //required
                conn.Close();
            }

            return errorMessage;
        }

        public User? GetByEmailAndPassword(string email, string password)
        {
            User? user = null;
            Dictionary<int, Role> rolesDictionary = new Dictionary<int, Role>();

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                // Retrieve roles and store them in the dictionary
                string roleSql = "SELECT * FROM roles";
                MySqlCommand roleCmd = new MySqlCommand(roleSql, conn);
                MySqlDataReader roleRdr = roleCmd.ExecuteReader();

                while (roleRdr.Read())
                {
                    int roleId = roleRdr.GetInt32("role_id");
                    string roleName = roleRdr.GetString("role_name");

                    Role role = new Role
                    {
                        RoleId = roleId,
                        RoleName = roleName
                    };

                    rolesDictionary.Add(roleId, role);
                }

                roleRdr.Close();

                // Retrieve users and assign roles
                string userSql = "SELECT user_id, email, role_id FROM users WHERE email=@Email and password=@Password";
                MySqlCommand userCmd = new MySqlCommand(userSql, conn);
                userCmd.Parameters.AddWithValue("@Email", email);
                userCmd.Parameters.AddWithValue("@Password", password);
                MySqlDataReader userRdr = userCmd.ExecuteReader();

                while (userRdr.Read())
                {
                    int userId = userRdr.GetInt32("user_id");
                    string uEmail = userRdr.GetString("email");
                    int roleId = userRdr.GetInt32("role_id");

                    user = new User
                    {
                        user_id = userId,
                        email = uEmail,
                        role_id = roleId,
                       
                    };

                    // Check if the role exists in the dictionary
                    if (rolesDictionary.TryGetValue(roleId, out Role userRole))
                    {
                        user.Role = userRole;
                    }

                }

                userRdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                //required
                conn.Close();
            }


            return user;
        }
    }
}
