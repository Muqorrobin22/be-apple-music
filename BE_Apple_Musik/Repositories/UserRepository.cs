﻿using BE_Apple_Musik.Models;
using MySql.Data.MySqlClient;
using static System.Net.Mime.MediaTypeNames;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http.HttpResults;

namespace BE_Apple_Musik.Repositories
{
    public class UserRepository
    {
        public string _connectionString;

        public UserRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();
            Dictionary<int, Role> rolesDictionary = new Dictionary<int, Role>();

            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                // Retrieve roles and store them in the dictionary
                string roleSql = "SELECT * FROM roles";
                MySqlCommand roleCmd = new MySqlCommand(roleSql, conn);
                MySqlDataReader roleRdr = roleCmd.ExecuteReader();

                while (roleRdr.Read())
                {
                    int roleId = roleRdr.GetInt32("role_id");
                    string roleName = roleRdr.GetString("role_name");

                    Role role = new Role
                    {
                        RoleId = roleId,
                        RoleName = roleName
                    };

                    rolesDictionary.Add(roleId, role);
                }

                roleRdr.Close();

                // Retrieve users and assign roles
                string userSql = "SELECT * FROM users";
                MySqlCommand userCmd = new MySqlCommand(userSql, conn);
                MySqlDataReader userRdr = userCmd.ExecuteReader();

                while (userRdr.Read())
                {
                    int userId = userRdr.GetInt32("user_id");
                    string firstName = userRdr.GetString("first_name");
                    string lastName = userRdr.GetString("last_name");
                    string email = userRdr.GetString("email");
                    string password = userRdr.GetString("password");
                    bool isActive = userRdr.GetBoolean("is_active");
                    int roleId = userRdr.GetInt32("role_id");

                    User user = new User
                    {
                        user_id = userId,
                        first_name = firstName,
                        last_name = lastName,
                        email = email,
                        password = password,
                        is_active = isActive,
                        role_id = roleId
                    };

                    // Check if the role exists in the dictionary
                    if (rolesDictionary.TryGetValue(roleId, out Role userRole))
                    {
                        user.Role = userRole;
                    }

                    users.Add(user);
                }

                userRdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
                Console.WriteLine("Done.");
            }

            return users;
        }

        public string? Create(string firstName, string lastName, string email, string password, bool is_active, DateTime activateDate,
            int role_id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO users (first_name, last_name, email, password, is_active, activation_date, role_id) VALUES (@firstName, @lastName, @email, @password, @isActive, @activateDate, @roleId )";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@firstName", firstName);
                cmd.Parameters.AddWithValue("@lastName", lastName);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@password", password);
                cmd.Parameters.AddWithValue("@isActive", is_active);
                cmd.Parameters.AddWithValue("@activateDate", activateDate);
                cmd.Parameters.AddWithValue("@roleId", role_id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if(rowsAffected < 1)
                {
                    return null;
                }

                return "Ok";

            }
            catch (Exception ex)
            {
                
                Console.WriteLine(ex.ToString());
                return "error";
            }
            finally
            {
                // required
                conn.Close();
            }
        }
    }
}
