﻿using BE_Apple_Musik.DTO.CourseSchedule;
using BE_Apple_Musik.DTO.Role;
using BE_Apple_Musik.Models;
using MySql.Data.MySqlClient;

namespace BE_Apple_Musik.Repositories
{
    public class CourseScheduleRepository
    {
        public string _connectionString;

        public CourseScheduleRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<CourseSchedule> GetAll()
        {
            List<CourseSchedule> scheduleCourses = new List<CourseSchedule>();
            Dictionary<int, Schedule> scheduleDictionary = new Dictionary<int, Schedule>();
            Dictionary<int, Course> courseDictionary = new Dictionary<int, Course>();
            Dictionary<int, Category> categoryDictionary = new Dictionary<int, Category>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string roleSql = "SELECT * FROM schedules";
                MySqlCommand roleCmd = new MySqlCommand(roleSql, conn);
                MySqlDataReader roleRdr = roleCmd.ExecuteReader();

                while (roleRdr.Read())
                {
                    int scheduleId = roleRdr.GetInt32("id");
                    DateTime scheduleTime = roleRdr.GetDateTime("schedule_time");

                    Schedule schedule = new Schedule
                    {
                        id = scheduleId,
                        schedule_time = scheduleTime,
                    };

                    scheduleDictionary.Add(scheduleId, schedule);
                }

                roleRdr.Close();

                string catSql = "SELECT * FROM category";
                MySqlCommand catCmd = new MySqlCommand(catSql, conn);
                MySqlDataReader catRdr = catCmd.ExecuteReader();

                while (catRdr.Read())
                {
                    int categoryId = catRdr.GetInt32("category_id");
                    string categoryName = catRdr.GetString("category_name");
                    string categoryDesc = catRdr.GetString("category_description");
                    string catIcon = catRdr.GetString("category_icon");
                    string catImage = catRdr.GetString("category_image");

                    Category cat = new Category
                    {
                        category_id = categoryId,
                        category_name = categoryName,
                        category_description = categoryDesc,
                        category_icon = catIcon,
                        category_image = catImage
                    };

                    categoryDictionary.Add(categoryId, cat);
                }

                catRdr.Close();

                string sql = "SELECT * FROM courses";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int courseId = rdr.GetInt32("id");
                    string title = rdr.GetString("title");
                    string desc = rdr.GetString("description");
                    decimal price = rdr.GetDecimal("price");
                    string image_link = rdr.GetString("image_link");
                    int category_id = rdr.GetInt32("category_id");

                    Course course = new Course
                    {
                        id = courseId,
                        title = title,
                        description = desc,
                        price = price,
                        image_link = image_link,
                        category_id = category_id,
                    };

                    if (categoryDictionary.TryGetValue(category_id, out Category courseCat))
                    {
                        course.category = courseCat;
                    }

                    courseDictionary.Add(courseId, course);

                }

                rdr.Close();

                string sqlcourseSchedule = "SELECT * FROM course_schedule";
                MySqlCommand cmdCourseSchedule = new MySqlCommand(sqlcourseSchedule, conn);
                MySqlDataReader rdrCourseSchedule = cmdCourseSchedule.ExecuteReader();

                while (rdrCourseSchedule.Read())
                {
                    int courseScheduleId = rdrCourseSchedule.GetInt32("id");
                    int courseId = rdrCourseSchedule.GetInt32("course_id");
                    int scheduleId = rdrCourseSchedule.GetInt32("schedule_id");


                    CourseSchedule scheduleCourse = new CourseSchedule
                    {
                        id=courseScheduleId,
                        course_id = courseId,
                        schedule_id = scheduleId
                    };

                    if (scheduleDictionary.TryGetValue(scheduleId, out Schedule scheduleInfo))
                    {
                        scheduleCourse.Schedule = scheduleInfo;
                    }

                    if (courseDictionary.TryGetValue(courseId, out Course courseInfo))
                    {
                        scheduleCourse.Course = courseInfo;
                    }

                    scheduleCourses.Add(scheduleCourse);

                }

                rdrCourseSchedule.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }

            return scheduleCourses;
        }


        public CourseSchedule? GetById(int id)
        {
            CourseSchedule? courseSchedule = null;
            Dictionary<int, Schedule> scheduleDictionary = new Dictionary<int, Schedule>();
            Dictionary<int, Course> courseDictionary = new Dictionary<int, Course>();
            Dictionary<int, Category> categoryDictionary = new Dictionary<int, Category>();

            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string roleSql = "SELECT * FROM schedules";
                MySqlCommand roleCmd = new MySqlCommand(roleSql, conn);
                MySqlDataReader roleRdr = roleCmd.ExecuteReader();

                while (roleRdr.Read())
                {
                    int scheduleId = roleRdr.GetInt32("id");
                    DateTime scheduleTime = roleRdr.GetDateTime("schedule_time");

                    Schedule schedule = new Schedule
                    {
                        id = scheduleId,
                        schedule_time = scheduleTime,
                    };

                    scheduleDictionary.Add(scheduleId, schedule);
                }

                roleRdr.Close();

                string catSql = "SELECT * FROM category";
                MySqlCommand catCmd = new MySqlCommand(catSql, conn);
                MySqlDataReader catRdr = catCmd.ExecuteReader();

                while (catRdr.Read())
                {
                    int categoryId = catRdr.GetInt32("category_id");
                    string categoryName = catRdr.GetString("category_name");
                    string categoryDesc = catRdr.GetString("category_description");
                    string catIcon = catRdr.GetString("category_icon");
                    string catImage = catRdr.GetString("category_image");

                    Category cat = new Category
                    {
                        category_id = categoryId,
                        category_name = categoryName,
                        category_description = categoryDesc,
                        category_icon = catIcon,
                        category_image = catImage
                    };

                    categoryDictionary.Add(categoryId, cat);
                }

                catRdr.Close();

                string sql = "SELECT * FROM courses";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int courseId = rdr.GetInt32("id");
                    string title = rdr.GetString("title");
                    string desc = rdr.GetString("description");
                    decimal price = rdr.GetDecimal("price");
                    string image_link = rdr.GetString("image_link");
                    int category_id = rdr.GetInt32("category_id");

                    Course course = new Course
                    {
                        id = courseId,
                        title = title,
                        description = desc,
                        price = price,
                        image_link = image_link,
                        category_id = category_id,
                    };

                    if (categoryDictionary.TryGetValue(category_id, out Category courseCat))
                    {
                        course.category = courseCat;
                    }

                    courseDictionary.Add(courseId, course);

                }

                rdr.Close();

                string sqlcourseSchedule = "SELECT * FROM course_schedule where id=@id";
                MySqlCommand cmdCourseSchedule = new MySqlCommand(sqlcourseSchedule, conn);
                cmdCourseSchedule.Parameters.AddWithValue("@id", id);
                MySqlDataReader rdrCourseSchedule = cmdCourseSchedule.ExecuteReader();

                while (rdrCourseSchedule.Read())
                {
                    int courseScheduleId = rdrCourseSchedule.GetInt32("id");
                    int courseId = rdrCourseSchedule.GetInt32("course_id");
                    int scheduleId = rdrCourseSchedule.GetInt32("schedule_id");


                    courseSchedule = new CourseSchedule
                    {
                        id = courseScheduleId,
                        course_id = courseId,
                        schedule_id = scheduleId
                    };

                    if (scheduleDictionary.TryGetValue(scheduleId, out Schedule scheduleInfo))
                    {
                        courseSchedule.Schedule = scheduleInfo;
                    }

                    if (courseDictionary.TryGetValue(courseId, out Course courseInfo))
                    {
                        courseSchedule.Course = courseInfo;
                    }


                }

                rdrCourseSchedule.Close();

                if (courseSchedule == null)
                {
                    // Set the response status code to 404 (Not Found).
                    return null;
                }

                return courseSchedule;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Oops Id tidak ditemukan, Gagal untuk menerima ID");
                return null;

            }
            finally
            {
                conn.Close();
            }

        }



        public string Create(CreateCourseScheduleDTO createCourseScheduleDTO)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO course_schedule (course_id, schedule_id) VALUES (@courseId, @scheduleId)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@courseId", createCourseScheduleDTO.course_id);
                cmd.Parameters.AddWithValue("@scheduleId", createCourseScheduleDTO.schedule_id);
                cmd.ExecuteNonQuery();

                return "Ok";

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "error";
            }
            finally
            {
                // required
                conn.Close();
            }

        }

        public string Update(int id, UpdateCourseScheduleDTO updateCourseScheduleDTO)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "update course_schedule SET course_id=@courseId, schedule_id = @scheduleId where id = @id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@courseId", updateCourseScheduleDTO.course_id);
                cmd.Parameters.AddWithValue("@scheduleId", updateCourseScheduleDTO.schedule_id);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    return "not_found";
                }

                return "Ok";



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "error";
            }
            finally
            {
                conn.Close();
            }

        }

        public bool Delete(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "DELETE FROM course_schedule where id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

            conn.Close();

            return true;
        }

    }
}
