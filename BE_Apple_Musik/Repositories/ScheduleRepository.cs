﻿using BE_Apple_Musik.DTO.Role;
using BE_Apple_Musik.DTO.Schedule;
using BE_Apple_Musik.Models;
using MySql.Data.MySqlClient;

namespace BE_Apple_Musik.Repositories
{
    public class ScheduleRepository
    {
        public string _connectionString;

        public ScheduleRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<Schedule> GetAll()
        {
            List<Schedule> schedules = new List<Schedule>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM schedules";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int id = rdr.GetInt32("id");
                    DateTime scheduleTime = rdr.GetDateTime("schedule_time");

                    schedules.Add(new Schedule
                    {
                        id = id,
                        schedule_time = scheduleTime
                    });

                }
                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }


            return schedules;
        }

        public Schedule GetById(int id)
        {
            Schedule schedule = null;

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM schedules where id = @scheduleId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@scheduleId", id);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int scheduleId = rdr.GetInt32("id");
                    DateTime scheduleDate = rdr.GetDateTime("schedule_time");

                    schedule = new Schedule
                    {
                        id = scheduleId,
                        schedule_time = scheduleDate
                    };

                }
                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }

            return schedule;
        }

        public string Create(CreateScheduleDTO createScheduleDTO)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO schedules (schedule_time) VALUES (@scheduleTime)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@scheduleTime", createScheduleDTO.schedule_course);
                cmd.ExecuteNonQuery();

                return "Ok";

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

                return "error";
            }
            finally
            {
                // required
                conn.Close();
            }

        }

        public string Update(int id, UpdateScheduleDTO updateScheduleDTO)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "update schedules SET schedule_time=@scheduleTime where id = @id;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@scheduleTime", updateScheduleDTO.schedule_courses);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    return "not_found";
                }

                return "Ok";



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "error";
            }
            finally
            {
                conn.Close();
            }

        }

        public bool Delete(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "DELETE FROM schedules where id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }
            finally
            {
                conn.Close();
            }

            return true;
        }
    }
}
