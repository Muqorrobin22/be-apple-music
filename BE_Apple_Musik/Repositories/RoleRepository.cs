﻿using BE_Apple_Musik.DTO.Role;
using BE_Apple_Musik.Models;
using MySql.Data.MySqlClient;
using static System.Net.Mime.MediaTypeNames;
using System.Xml.Linq;

namespace BE_Apple_Musik.Repositories
{
    public class RoleRepository
    {
        public string _connectionString;

        public RoleRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<Role> GetAll()
        {
            List<Role> categories = new List<Role>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM roles";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int id = rdr.GetInt32("role_id");
                    string role_name = rdr.GetString("role_name");

                    categories.Add(new Role
                    {
                        RoleId = id,
                        RoleName = role_name
                    });

                }
                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            Console.WriteLine("Done.");

            return categories;
        }

        public string Create(CreateRoleDTO createRoleDTO)
        {
            string errorMessage = string.Empty;
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO roles (role_name) VALUES (@roleName)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@roleName", createRoleDTO.RoleName);
                cmd.ExecuteNonQuery();

                return "Ok";

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.ToString());

                return "error";
            }
            finally
            {
                // required
                conn.Close();
            }

        }


        public Role GetById(int id)
        {
            Role role = null;

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "SELECT * FROM roles where role_id = @roleId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@roleId", id);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int roleId = rdr.GetInt32("role_id");
                    string role_name = rdr.GetString("role_name");

                    role = new Role
                    {
                        RoleId = id,
                        RoleName = role_name
                    };

                }
                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conn.Close();
            Console.WriteLine("Done.");

            return role;
        }

        public string Update(int id, UpdateRoleDTO updateRoleDTO)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "update roles SET role_name=@roleName where role_id = @id;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@roleName", updateRoleDTO.RoleName);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if(rowsAffected < 1)
                {
                    return "not_found";
                }

                return "Ok";



            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "error";
            }
            finally
            {
                conn.Close();
            }

        }

    }
}
