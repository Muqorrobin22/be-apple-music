﻿using BE_Apple_Musik.Models;
using MySql.Data.MySqlClient;
using static System.Net.Mime.MediaTypeNames;

namespace BE_Apple_Musik.Repositories
{
    public class CourseRepository
    {
        public string _connectionString;

        public CourseRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }

        public List<Course> GetAll()
        {
            List<Course> courses = new List<Course>();
            Dictionary<int, Category> categoryDictionary = new Dictionary<int, Category>();

            //string connStr = "server=localhost;user=root;database=apple_musik;port=3308;password=root";
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string roleSql = "SELECT * FROM category";
                MySqlCommand roleCmd = new MySqlCommand(roleSql, conn);
                MySqlDataReader roleRdr = roleCmd.ExecuteReader();

                while (roleRdr.Read())
                {
                    int categoryId = roleRdr.GetInt32("category_id");
                    string categoryName = roleRdr.GetString("category_name");
                    string categoryDesc = roleRdr.GetString("category_description");
                    string catIcon = roleRdr.GetString("category_icon");
                    string catImage = roleRdr.GetString("category_image");

                    Category cat = new Category
                    {
                        category_id = categoryId,
                        category_name = categoryName,
                        category_description = categoryDesc,
                        category_icon = catIcon,
                        category_image =  catImage
                    };

                    categoryDictionary.Add(categoryId, cat);
                }

                roleRdr.Close();

                string sql = "SELECT * FROM courses";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int id = rdr.GetInt32("id");
                    string title = rdr.GetString("title");
                    string desc = rdr.GetString("description");
                    decimal price = rdr.GetDecimal("price");
                    string image_link = rdr.GetString("image_link");
                    int category_id = rdr.GetInt32("category_id");

                    Course course = new Course
                    {
                        id = id,
                        title = title,
                        description = desc,
                        price = price,
                        image_link = image_link,
                        category_id = category_id,
                    };

                    if (categoryDictionary.TryGetValue(category_id, out Category courseCat))
                    {
                        course.category = courseCat;
                    }

                    courses.Add(course);

                }

                rdr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }

            return courses;
        }

        public Course? GetById(int id)
        {
            Course? course = null;
            Dictionary<int, Category> categoryDictionary = new Dictionary<int, Category>();

            MySqlConnection conn = new MySqlConnection(_connectionString);

            try
            {
                conn.Open();

                string roleSql = "SELECT * FROM category";
                MySqlCommand roleCmd = new MySqlCommand(roleSql, conn);
                MySqlDataReader roleRdr = roleCmd.ExecuteReader();

                while (roleRdr.Read())
                {
                    int categoryId = roleRdr.GetInt32("category_id");
                    string categoryName = roleRdr.GetString("category_name");
                    string categoryDesc = roleRdr.GetString("category_description");
                    string catIcon = roleRdr.GetString("category_icon");
                    string catImage = roleRdr.GetString("category_image");

                    Category cat = new Category
                    {
                        category_id = categoryId,
                        category_name = categoryName,
                        category_description = categoryDesc,
                        category_icon = catIcon,
                        category_image = catImage
                    };

                    categoryDictionary.Add(categoryId, cat);
                }

                roleRdr.Close();

                string sql = "SELECT * FROM courses WHERE id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int course_id = rdr.GetInt32("id");
                    string title = rdr.GetString("title");
                    string desc = rdr.GetString("description");
                    decimal price = rdr.GetDecimal("price");
                    string image_link = rdr.GetString("image_link");
                    int category_id = rdr.GetInt32("category_id");

                    course = new Course
                    {
                        id = course_id,
                        title = title,
                        description = desc,
                        price = price,
                        image_link = image_link,
                        category_id = category_id,
                    };

                    if (categoryDictionary.TryGetValue(category_id, out Category courseCat))
                    {
                        course.category = courseCat;
                    }


                }

                rdr.Close();

                if (course == null)
                {
                    // Set the response status code to 404 (Not Found).
                    return null;
                }

                return course;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Oops Id tidak ditemukan, Gagal untuk menerima ID");
                return null;

            }finally
            {
                conn.Close();
            }

        }

        public string Create(string title, string description, decimal price, string image_link, int category_id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO courses (title, description, price, image_link, category_id) VALUES (@title, @description, @price, @imageLink, @category_id )";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@description", description);
                cmd.Parameters.AddWithValue("@price", price);
                cmd.Parameters.AddWithValue("@imageLink", image_link);
                cmd.Parameters.AddWithValue("@category_id", category_id);
                cmd.ExecuteNonQuery();

                return "Ok";

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "failed";
            }
            finally
            {
                // required
                conn.Close();
            }

        }

        public string Update(int id, string title, string description, decimal price, string image_link, int category_id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "update courses set title=@title, description=@description, price=@price, image_link=@imageLink, category_id=@category_id where id=@id;";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@description", description);
                cmd.Parameters.AddWithValue("@price", price);
                cmd.Parameters.AddWithValue("@imageLink", image_link);
                cmd.Parameters.AddWithValue("@category_id", category_id);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    return "not_found";
                }

                return "Ok";

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return "failed";
            }
            finally
            {
                // required
                conn.Close();
            }

        }

        public bool Delete(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "DELETE FROM courses where id=@id";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@id", id);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

            conn.Close();

            return true;
        }

    }
}
