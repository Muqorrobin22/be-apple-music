﻿using BE_Apple_Musik.DTO.Role;
using BE_Apple_Musik.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BE_Apple_Musik.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly RoleRepository _roleRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public RoleController(RoleRepository roleRepository, IWebHostEnvironment webHostEnvironment)
        {
            _roleRepository = roleRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_roleRepository.GetAll());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var roleById = _roleRepository.GetById(id);

                if(roleById == null)
                {
                    return NotFound();
                }

                return Ok(roleById);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult CreateRole([FromBody] CreateRoleDTO createRoleDTO)
        {
            try
            {
                string createRole = _roleRepository.Create(createRoleDTO);

                if(createRole == "error")
                {
                    return BadRequest("Failed to Create Role");
                }


                return Ok("create Role success");

            }catch (Exception ex)
            {
                return StatusCode(500, "Failed");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPatch("{id}")]
        public IActionResult UpdateRole(int id, [FromBody] UpdateRoleDTO updateRoleDTO)
        {
            try
            {
                string updateRole = _roleRepository.Update(id, updateRoleDTO);

                if(updateRole == "error")
                {
                    return BadRequest("Failed to Update the Role");
                }

                if(updateRole == "not_found")
                {
                    return NotFound();
                }

                return Ok($"Update Role with id: {id} Success");

            } catch(Exception ex)
            {
                return StatusCode(500, "Failed");
            }
        }


    }
}
