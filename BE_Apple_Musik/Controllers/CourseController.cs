﻿using BE_Apple_Musik.DTO.Category;
using BE_Apple_Musik.DTO.Course;
using BE_Apple_Musik.Models;
using BE_Apple_Musik.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BE_Apple_Musik.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly CourseRepository _courseRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CourseController(CourseRepository courseRepository, IWebHostEnvironment webHostEnvironment)
        {
            _courseRepository = courseRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_courseRepository.GetAll());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var cat = _courseRepository.GetById(id);
                if (cat == null)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateCourse([FromForm] CreateCourseDTO createCourseDto)
        {

            IFormFile image = createCourseDto.image!;

            // TODO: save image to server
            var extIcon = Path.GetExtension(image.FileName).ToLowerInvariant(); //.jpg

            //get filename
            string fileNameIcon = Guid.NewGuid().ToString() + extIcon; //pasti unik
            string uploadDir = "uploads"; //foldering biar rapih
            string physicalPath = $"wwwroot/{uploadDir}";
            //saving image
            var filePathIcon = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, fileNameIcon);// 'D:\Coding ID\Apple Music\BE_Apple_Musik\BE_Apple_Musik\wwwroot\uploads\f319a243-9d6d-4ea1-9ebe-7e7264828b22.jpg'.
            using var streamIcon = System.IO.File.Create(filePathIcon);
            await image.CopyToAsync(streamIcon);

            //create url path
            string fileUrlPathIcon = $"{uploadDir}/{fileNameIcon}";



            var course = _courseRepository.Create(createCourseDto.title,
                createCourseDto.description,
                createCourseDto.price,
                filePathIcon,
                createCourseDto.category_id
            );

            if (course == "failed")
            {
                // berarti ada error
                return Problem(course);
            }

            return Ok(course);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Update(int id, [FromForm] UpdateCourseDTO updateCourseDto)
        {

            IFormFile image = updateCourseDto.image!;

            // TODO: save image to server
            var extIcon = Path.GetExtension(image.FileName).ToLowerInvariant(); //.jpg

            //get filename
            string fileNameIcon = Guid.NewGuid().ToString() + extIcon; //pasti unik
            string uploadDir = "uploads"; //foldering biar rapih
            string physicalPath = $"wwwroot/{uploadDir}";
            //saving image
            var filePathIcon = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, fileNameIcon);// 'D:\Coding ID\Apple Music\BE_Apple_Musik\BE_Apple_Musik\wwwroot\uploads\f319a243-9d6d-4ea1-9ebe-7e7264828b22.jpg'.
            using var streamIcon = System.IO.File.Create(filePathIcon);
            await image.CopyToAsync(streamIcon);

            //create url path
            string fileUrlPathIcon = $"{uploadDir}/{fileNameIcon}";



            var course = _courseRepository.Update( id,
                updateCourseDto.title,
                updateCourseDto.description,
                updateCourseDto.price,
                filePathIcon,
                updateCourseDto.category_id
            );

            if (course == "not_found")
            {
                return NotFound();
            }

            if (course == "failed")
            {
                // berarti ada error
                return Problem(course);
            }

            return Ok(course);
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var cat = _courseRepository.Delete(id);
                if (cat == false)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }
    }
}
