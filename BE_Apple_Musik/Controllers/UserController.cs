﻿using BE_Apple_Musik.DTO;
using BE_Apple_Musik.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BE_Apple_Musik.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserRepository _userRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public UserController(UserRepository userRepository, IWebHostEnvironment webHostEnvironment)
        {
            _userRepository = userRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_userRepository.GetAll());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }


        [Authorize(Roles = "Admin")]
        [HttpPost] 
        public IActionResult Create([FromBody] CreateUserDTO createUserDTO)
        {
            try
            {
                string? createdUser = _userRepository.Create(createUserDTO.first_name, createUserDTO.last_name, createUserDTO.email, createUserDTO.password, createUserDTO.is_active, createUserDTO.activation_date, createUserDTO.role_id);

            
                if(createdUser == "error")
                {
                    return BadRequest("Failed to Create User");
                }

                return Ok("Create Users Success");

            } catch (Exception)
            {
                return StatusCode(500, "Failed");
            }
        }
    }
}
