﻿using BE_Apple_Musik.DTO.Category;
using BE_Apple_Musik.Models;
using BE_Apple_Musik.Repositories;
using BE_Apple_Musik.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace BE_Apple_Musik.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryRepository _categoryRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;
        
        public CategoryController(CategoryRepository categoryRepository, IWebHostEnvironment webHostEnvironment)
        {
            _categoryRepository = categoryRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {

                return Ok(_categoryRepository.GetAll());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var cat = _categoryRepository.GetById(id);
                if(cat == null)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> CreateCategory([FromForm] AddCategoryDTO addTodoDto)
        {

            IFormFile categoryIcon = addTodoDto.category_icon!;
            IFormFile categoryImage = addTodoDto.category_image!;

            // TODO: save image to server
            var extIcon = Path.GetExtension(categoryIcon.FileName).ToLowerInvariant(); //.jpg
            var extImage = Path.GetExtension(categoryImage.FileName).ToLowerInvariant();

            //get filename
            string fileNameIcon = Guid.NewGuid().ToString() + extIcon; //pasti unik
            string fileNameImage = Guid.NewGuid().ToString() + extImage; //pasti unik
            string uploadDir = "uploads"; //foldering biar rapih
            string physicalPath = $"wwwroot/{uploadDir}";
            //saving image
            var filePathIcon = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, fileNameIcon);
            var filePathImage = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, fileNameImage); // 'D:\Coding ID\Apple Music\BE_Apple_Musik\BE_Apple_Musik\wwwroot\uploads\f319a243-9d6d-4ea1-9ebe-7e7264828b22.jpg'.
            using var streamIcon = System.IO.File.Create(filePathIcon);
            using var streamImage = System.IO.File.Create(filePathImage);
            await categoryIcon.CopyToAsync(streamIcon);
            await categoryImage.CopyToAsync(streamImage);

            //create url path
            string fileUrlPathIcon = $"{uploadDir}/{fileNameIcon}";
            string fileUrlPathImage = $"{uploadDir}/{fileNameImage}";



            var category = _categoryRepository.Create(addTodoDto.category_name,
                addTodoDto.category_description,
                filePathIcon,
                 filePathImage
            );

            if (!string.IsNullOrEmpty(category))
            {
                // berarti ada error
                return Problem(category);
            }

            return Ok(category);
        }

        [Authorize(Roles = "Admin")]
        [HttpPatch("{id}")]
        public async Task<ActionResult> UpdateCategory(int id, [FromForm] UpdateCategoryDTO updateCateogryDto)
        {

            IFormFile categoryIcon = updateCateogryDto.category_icon!;
            IFormFile categoryImage = updateCateogryDto.category_image!;

            // TODO: save image to server
            var extIcon = Path.GetExtension(categoryIcon.FileName).ToLowerInvariant(); //.jpg
            var extImage = Path.GetExtension(categoryImage.FileName).ToLowerInvariant();

            //get filename
            string fileNameIcon = Guid.NewGuid().ToString() + extIcon; //pasti unik
            string fileNameImage = Guid.NewGuid().ToString() + extImage; //pasti unik
            string uploadDir = "uploads"; //foldering biar rapih
            string physicalPath = $"wwwroot/{uploadDir}";
            //saving image
            var filePathIcon = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, fileNameIcon);
            var filePathImage = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, fileNameImage); // 'D:\Coding ID\Apple Music\BE_Apple_Musik\BE_Apple_Musik\wwwroot\uploads\f319a243-9d6d-4ea1-9ebe-7e7264828b22.jpg'.
            using var streamIcon = System.IO.File.Create(filePathIcon);
            using var streamImage = System.IO.File.Create(filePathImage);
            await categoryIcon.CopyToAsync(streamIcon);
            await categoryImage.CopyToAsync(streamImage);

            //create url path
            string fileUrlPathIcon = $"{uploadDir}/{fileNameIcon}";
            string fileUrlPathImage = $"{uploadDir}/{fileNameImage}";

            
                var category = _categoryRepository.Update(
                    id,
                    updateCateogryDto.category_name,
                    updateCateogryDto.category_description,
                    filePathIcon,
                    filePathImage
                );

            if (category == "not_found")
            {
                return NotFound();
            }

            if (!string.IsNullOrEmpty(category))
            {
                // berarti ada error
                return Problem(category);
            }

            return Ok(category);
            
            
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id:int}")]
        public IActionResult DeleteCategory(int id)
        {
            try
            {
                var cat = _categoryRepository.Delete(id);
                if (cat == false)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error Retrieving data from the database");
            }
        }
    }
}
