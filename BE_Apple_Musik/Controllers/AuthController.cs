﻿using BE_Apple_Musik.DTO.Auth;
using BE_Apple_Musik.Helpers;
using BE_Apple_Musik.Models;
using BE_Apple_Musik.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace BE_Apple_Musik.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly AuthRepository _authRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public AuthController(AuthRepository authRepository, IWebHostEnvironment webHostEnvironment)
        {
            _authRepository = authRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("/Register")]
        public async Task<ActionResult> RegisterUser([FromBody] RegisterUserDTO data)
        {
            string hashedPassword = PasswordHelper.HashPassword(data.Password);
            string verificationToken = Guid.NewGuid().ToString();

            // check if email already taken
            User? user = _authRepository.GetByEmail(data.Email);
            if (user != null)
            {
                return BadRequest("Email Already Taken");
            }

            string errorMessage = _authRepository.Register(data, hashedPassword, verificationToken);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                return Problem("Error when registering user");
            }

            //send email
//            string htmlEmail = $@"
//Hello <b>{data.Email}</b>, please click link below to verify<br/>
//<a href='http://localhost:5173/verify/{verificationToken}'>Verify My Account</a>
//";
//            await MailHelper.Send("Dear User", data.Email, "Email Verification", htmlEmail);


            return Ok();

        }

        [HttpPost("/Login")]
        public ActionResult Login([FromBody] LoginUserDTO data)
        {
            string hashedPassword = PasswordHelper.HashPassword(data.Password);

            User? user = _authRepository.GetByEmailAndPassword(data.Email, hashedPassword);
            if (user == null)
            {
                return NotFound();
            }

            //create token
            string token = JWTHelper.Generate(user.user_id, user.Role.RoleName);

            return Ok(new { Token = token, Role = user.Role.RoleName });
        }
    }
}
