﻿using System.ComponentModel;

namespace BE_Apple_Musik.DTO.Auth
{
    public class RegisterUserDTO
    {
        [DefaultValue("muqorrobin.ahmad22@gmail.com")]
        public string Email { get; set; } = string.Empty;
        [DefaultValue("12345678")]
        public string Password { get; set; } = string.Empty;
        public string first_name { get; set; } = string.Empty;
        public string last_name { get; set;} = string.Empty;
    }
}
