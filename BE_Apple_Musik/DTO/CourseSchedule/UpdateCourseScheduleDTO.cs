﻿namespace BE_Apple_Musik.DTO.CourseSchedule
{
    public class UpdateCourseScheduleDTO
    {
        public int course_id { get; set; }
        public int schedule_id { get; set; }
    }
}
