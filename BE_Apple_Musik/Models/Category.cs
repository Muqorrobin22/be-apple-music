﻿namespace BE_Apple_Musik.Models
{
    public class Category
    {
        public int category_id { get; set; }
        public string category_name { get; set; } = string.Empty;
        public string category_description { get; set; } = string.Empty;
        public string? category_icon { get; set; } = string.Empty;
        public string? category_image { get; set; } = string.Empty;

    }
}
