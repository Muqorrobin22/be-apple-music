﻿namespace BE_Apple_Musik.Models
{
    public class CourseSchedule
    {
        public int id {  get; set; }
        public int course_id { get; set; }
        public int schedule_id { get; set; }
        public Course Course { get; set; }
        public Schedule Schedule { get; set; }
    }
}
