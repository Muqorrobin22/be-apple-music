﻿namespace BE_Apple_Musik.Models
{
    public class Course
    {
        public int id {  get; set; }
        public string title { get; set; } = string.Empty;
        public string description { get; set; } = string.Empty;
        public decimal price { get; set; }
        public string image_link { get; set; } = string.Empty;
        public int category_id {  get; set; }
        public Category category { get; set; }

    }
}
