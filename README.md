# Untuk repository desain BE Apple Music

## Technology :

- ASP.NET Core 8
- ADO.NET (MySQL String Connector)
- Mysql

## Database Design

List of Database Design. Created and Updated Version for tracking Apple-Musik E-commerce Course Project.

Database Design Create on : **21 Desember 2023** (still need discussion).

- [x] abstract/still rough design

![alt text](db_design/images_21_dec_2023/database.jpg "Database Design 21 Desember 2023")

### UPDATE DATABASE DESIGN

Database Design Update on : **22 Desember 2023** (after discussion).

Major Update:

- [x] Create **ROLES** Table that have relationship _one to many_ with **Users** table
- [x] Separate of concern Between **Cart** Tables and **Invoice** Table with creating of **Orders** Table and **OrderDetails** Table
- [x] Change name of **class_detail** Table to **Courses** Table and **category_class** to **Category** Table
- [x] Add more Column with relevan data in each Table

![alt text](db_design/images_22_dec_2023/database_design.png "Database Design Major Update 22 Desember 2023")

### UPDATE DATABASE DESIGN

Database Design Update on : **22 Desember 2023**.

Minor Update :

- [x] add _schedule_ column in **Courses** Table
- [x] add _is_active_ column in **Users** Table

![alt text](db_design/images_22_dec_2023_update/database.png "Database Design Minor Update 22 Desember 2023")

### UPDATE DATABASE DESIGN

Database Design Update on : **26 Desember 2023**.

Major Update :

- [x] add _verification_token_ column in **Users** Table
- [x] add _verification_expired_date_ column in **Users** Table
- [x] delete _schedule_ column in **Courses** Table, and make it independent Tables in **Schedules** tables and make its relationship _many to many_
- [x] _many to many_ relationship between **Courses** with **Schedules** happends in **course_schedule** Table

![alt text](db_design/images_27_dec_2023/diagram_update.png "Database Design Minor Update 22 Desember 2023")
